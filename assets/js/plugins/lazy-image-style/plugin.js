/**
 * @file
 * Lazy image style CKEditor plugin.
 *
 * Basic plugin inserting abbreviation elements into the CKEditor editing area.
 *
 * @DCG The code is based on an example from CKEditor Plugin SDK tutorial.
 *
 * @see http://docs.ckeditor.com/#!/guide/plugin_sdk_sample_1
 */
(function (Drupal) {

  'use strict';

  CKEDITOR.plugins.add('lazy_image_style', {
    icons: 'lazy-image-style',
    init: function (editor) {
      editor.addCommand('lazyImageStyle', {
        contextSensitive: 1,
        exec: function (editor) {

          let elem = editor.getSelection().getStartElement();
          let imgs = elem.find('img.cke_widget_element');

          if (imgs.count()) {
            imgs.toArray().forEach((item) => {
              if (item.hasAttribute('data-use-lazy')) {
                item.removeAttribute('data-use_lazy');
                this.setState(CKEDITOR.TRISTATE_OFF);
                return;
              }
              item.setAttribute('data-use-lazy', true);
              this.setState(CKEDITOR.TRISTATE_ON);
            });
          }
        },
        refresh: function (editor, path) {
          let elems = path.lastElement.find('img[data-use-lazy]');
          if (elems.count()){
            this.setState(CKEDITOR.TRISTATE_ON);
            return;
          }
          this.setState(CKEDITOR.TRISTATE_OFF);
        }
      });
      editor.ui.addButton('lazy-image-style', {
        label: Drupal.t('Use lazy load'),
        command: 'lazyImageStyle',
        toolbar: 'insert'
      });

    }
  });

}(Drupal));
