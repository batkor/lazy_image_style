<?php

namespace Drupal\lazy_image_style;

/**
 * The twig extension.
 */
class TwigExtension extends \Twig_Extension {

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new \Twig_SimpleFunction('lazysizes', [$this, 'imageLazySizes']),
    ];
  }

  /**
   * Returns renderable image array for use lazysizes.
   *
   * @param string $src
   *   The src attribute, path to image.
   *
   * @param array $attributes
   *   The img tag attributes.
   *
   * @return array
   *   The renderable image array.
   */
  public function imageLazySizes(string $src, array $attributes = []): array {
    if (empty($attributes['class'])) {
      $attributes['class'] = [];
    }
    $attributes['class'][] = 'lazyload';
    return [
      '#theme' => 'image',
      '#uri' => $src,
      '#attached' => ['library' => ['lazy_image_style/main']],
      '#attributes' => [
        'srcset' => LazyImageStyleHelper::TMP_BASE64_IMAGE,
        'data-srcset' => $src,
      ] + $attributes,
    ];
  }

}
