<?php

namespace Drupal\lazy_image_style;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\image\ImageStyleInterface;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesserInterface;

/**
 * The helper service for alter build image.
 */
class LazyImageStyleHelper {

  /**
   * The default temporary base64 image.
   */
  const TMP_BASE64_IMAGE = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=";

  /**
   * The lazy_image_style ImmutableConfig.
   *
   * @var array
   */
  protected $settings;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The MIME type guesser.
   *
   * @var \Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesserInterface
   */
  protected $mimeTypeGuesser;

  /**
   * The image style storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $imageStyleStorage;

  /**
   * Constructs a lazyimagestylehelper object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesserInterface $mimeTypeGuesser
   *   The MIME type guesser.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    MimeTypeGuesserInterface $mimeTypeGuesser
  ) {

    $this->configFactory = $config_factory;
    $this->mimeTypeGuesser = $mimeTypeGuesser;

    $this->imageStyleStorage = $entity_type_manager->getStorage('image_style');
  }

  /**
   * Rebuild image render array for lazy load.
   *
   * @param array $variables
   *   An associative array containing:
   *   - image:
   *   - style_name:.
   */
  public function rebuild(array &$variables) {

    // Check use lazy image for current style.
    if (!$this->useLazyLoad('origin')) {
      return;
    }

    // Move source uri to data-src attribute.
    $variables['attributes']['data-srcset'] = file_url_transform_relative($variables['attributes']['src']);

    $variables['attributes']['srcset'] = $this->getTmpUrl($variables['uri']);
    $variables['attributes']['class'][] = 'lazyload';
    $variables['#attached'] = $this->addAttachments(isset($variables['#attached']) ? $variables['#attached'] : []);
  }
  

  /**
   * Rebuild image style render array for lazy load.
   *
   * @param array $variables
   *   An associative array containing:
   *   - image:
   *   - style_name:.
   */
  public function rebuildStyle(array &$variables) {

    // Check use lazy image for current style.
    if (!$this->useLazyLoad($variables['style_name'])) {
      return;
    }

    // Move source uri to data-src attribute.
    $variables['image']['#attributes']['data-srcset'] = file_url_transform_relative($variables['image']['#uri']);

    $variables['image']['#attributes']['srcset'] = $this->getTmpUrl($variables['uri']);
    $variables['image']['#attributes']['class'][] = 'lazyload';
    $variables['#attached'] = $this->addAttachments(isset($variables['#attached']) ? $variables['#attached'] : []);
  }

  /**
   * Rebuild responsive image render array.
   *
   * @param array $variables
   *   An associative array containing:
   *   - responsive_image_style_id:
   *   - img_element:.
   */
  public function rebuildResponsive(array &$variables) {
    // Check use lazy image for current style.
    if (!$this->useLazyLoad($variables['responsive_image_style_id'], 'responsive_image_use')) {
      return;
    }

    /** @var \Drupal\Core\Template\Attribute $source */
    foreach ($variables['sources'] as $source) {
      $srcset = $source->offsetGet('srcset');
      $source->removeAttribute('srcset');
      $source->setAttribute('data-srcset', $srcset);
    }

    $variables['img_element']['#attributes']['data-srcset'] = file_url_transform_relative($variables['img_element']['#uri']);
    $variables['img_element']['#attributes']['srcset'] = $this->getTmpUrl($variables['uri']);
    $variables['img_element']['#attributes']['class'][] = 'lazyload';
    $variables['#attached'] = $this->addAttachments(isset($variables['#attached']) ? $variables['#attached'] : []);
  }

  /**
   * Return array attachments.
   *
   * @param array $attached
   *   Attached array.
   *
   * @return array
   *   New attached array.
   */
  public function addAttachments(array $attached = []) {
    $attached['library'][] = 'lazy_image_style/main';
    $attached['drupalSettings']['lazy_image_style'] = [
      'offset' => $this->getConfigValue('offset') ?: '400',
    ];

    return $attached;
  }

  /**
   * Return tmp url image.
   *
   * @param string $source_uri
   *   Source uri.
   *
   * @return string
   *   the tmp url image.
   */
  protected function getTmpUrl($source_uri) {
    $lazy_style = $this->getConfigValue('lazy_style');

    if ($lazy_style == 'base64') {
      return self::TMP_BASE64_IMAGE;
    }

    $lazy_image_style = $this->loadStyle($lazy_style);
    if ($lazy_image_style instanceof ImageStyleInterface) {

      // Returns tmp image url if convert to base64.
      if (!$this->getConfigValue('convert')) {
        return $lazy_image_style->buildUrl($source_uri);
      }

      $tmp_image_uri = $lazy_image_style->buildUri($source_uri);

      // Create derivative if necessary.
      if (!file_exists($tmp_image_uri)) {
        $image_derivative = $lazy_image_style->createDerivative($source_uri, $tmp_image_uri);

        // If the image derivative could not be generated
        // Return tmp base64.
        if (!$image_derivative) {
          return self::TMP_BASE64_IMAGE;
        }
      }

      $mime = $this->mimeTypeGuesser->guess($tmp_image_uri);
      $base_64_image = base64_encode(file_get_contents($tmp_image_uri));

      return "data:{$mime};base64,{$base_64_image}";

    }

    return self::TMP_BASE64_IMAGE;
  }

  /**
   * The method for replace image style if deleted.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @see lazy_image_style_form_image_style_delete_form_alter
   */
  public function replaceImageStyle(array &$form, FormStateInterface $form_state) {
    $replace_style = empty($form_state->getValue('replacement')) ?
      'base64' : $form_state->getValue('replacement');

    $this
      ->configFactory
      ->getEditable('lazy_image_style.settings')
      ->set('lazy_style', $replace_style)
      ->save();
  }

  /**
   * Gets data from lazy_image_style.settings configuration object.
   *
   * @param string $key
   *   The config value key.
   *
   * @return mixed
   *   The data that was requested.
   */
  protected function getConfigValue($key) {
    return $this
      ->configFactory
      ->get('lazy_image_style.settings')
      ->get($key);
  }

  /**
   * Returns style entity from static.
   *
   * @param string $style_name
   *   The Style name.
   *
   * @return \Drupal\Core\Entity\EntityInterface|mixed|null
   *   The style entity.
   */
  protected function loadStyle($style_name) {
    $style = &drupal_static(__FUNCTION__ . ':' . $style_name);
    if ($style) {
      return $style;
    }
    $style = $this
      ->imageStyleStorage
      ->load($style_name);

    return $style;
  }

  /**
   * Return true if image style use lazy load or false.
   *
   * @param string $style_name
   *   This image style name or responsive image style id.
   * @param string $key
   *   This key in config.
   *
   * @return bool
   *   The status TRUE/FALSE.
   */
  protected function useLazyLoad($style_name, $key = 'use') {
    $style_name = $style_name ?: 'origin';
    $status = &drupal_static(__FUNCTION__ . ':' . $style_name);
    if ($status) {
      return $status;
    }

    $used_styles = $this->getConfigValue($key);
    $status = is_array($used_styles) && array_key_exists($style_name, $used_styles);

    return $status;
  }

}
