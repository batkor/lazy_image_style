<?php

namespace Drupal\lazy_image_style\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Lazy image settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The entity_type.manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The cache_tags.invalidator service.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidator
   */
  protected $cacheTagsInvalidator;

  /**
   * Returns the module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'lazy_image_style_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['lazy_image_style.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->cacheTagsInvalidator = $container->get('cache_tags.invalidator');
    $instance->moduleHandler = $container->get('module_handler');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('lazy_image_style.settings');

    $image_styles = image_style_options(FALSE);

    $lazy_style_options = array_merge($image_styles, [
      'base64' => $this->t('Use base64'),
    ]);

    $form['lazy_style'] = [
      '#type' => 'select',
      '#title' => $this->t('Temporary style'),
      '#description' => $this->t('This temporary style used before an image is visible.'),
      '#options' => $lazy_style_options,
      '#default_value' => $config->get('lazy_style'),
    ];

    // Description for base64.
    $form['base64_desc'] = [
      '#type' => 'container',
      '#states' => [
        'visible' => [
          ':input[name="lazy_style"]' => ['value' => 'base64'],
        ],
      ],
      [
        '#markup' => $this->t('<strong>Used image white color, 1x1 size and base64 format.</strong>'),
      ],
    ];
    $form['convert'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Convert image to base64 format'),
      '#default_value' => $config->get('convert'),
      '#description' => $this->t('Every temporary image converted to base64 format.'),
      '#states' => [
        'invisible' => [
          ':input[name="lazy_style"]' => ['value' => 'base64'],
        ],
      ],
    ];

    $form['offset'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Offset'),
      '#default_value' => $config->get('offset') ?: '400',
      '#description' => $this->t("The offset controls how early you want the elements to be loaded before they're visible. Default is <strong>400</strong>, so <strong>400px</strong> before an element is visible it'll start loading."),
      '#field_suffix' => 'px',
      '#maxlength' => 5,
      '#size' => 10,
    ];

    $form['use_group'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Used styles'),
      '#description' => $this->t('Selected style for use lazy image.'),
    ];

    $form['use_group']['use'] = [
      '#type' => 'checkboxes',
      '#options' => $image_styles + ['origin' => $this->t('Origin image.')],
      '#default_value' => $config->get('use') ?: [],
    ];

    if ($this->moduleHandler->moduleExists('responsive_image')) {
      $form['responsive_image_use_group'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Responsive image styles'),
        '#description' => $this->t('Selected style for use lazy image.'),
      ];

      $form['responsive_image_use_group']['responsive_image_use'] = [
        '#type' => 'checkboxes',
        '#options' => $this->getResponsiveImageStyles(),
        '#default_value' => $config->get('responsive_image_use') ?: [],
      ];
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('lazy_image_style.settings');

    // Image styles.
    $old_used = $config->get('use') ?: [];
    $new_used = array_filter($form_state->getValue('use', []));

    // Image responsive styles.
    $old_responsive = $config->get('responsive_image_use') ?: [];
    $new_responsive = array_filter($form_state->getValue('responsive_image_use', []));

    $config->set('use', $new_used)
      ->set('responsive_image_use', $new_responsive)
      ->set('lazy_style', $form_state->getValue('lazy_style'))
      ->set('convert', $form_state->getValue('convert'))
      ->set('offset', $form_state->getValue('offset'))
      ->save();

    // Invalidate tag cache.
    $tags = array_map(function ($style) {
      return 'lazy_image_style:' . $style;
    }, $old_used + $new_used + $old_responsive + $new_responsive);
    $this->cacheTagsInvalidator->invalidateTags($tags);

    parent::submitForm($form, $form_state);
  }

  /**
   * Returns all responsive image styles.
   *
   * @return array
   *   The all responsive image styles.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getResponsiveImageStyles() {
    $output = [];
    $responsive_styles = $this
      ->entityTypeManager
      ->getStorage('responsive_image_style')
      ->loadMultiple();
    foreach ($responsive_styles as $responsive_style) {
      $output[$responsive_style->id()] = $responsive_style->label();
    }
    return $output;
  }

}
