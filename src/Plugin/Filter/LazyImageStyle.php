<?php

namespace Drupal\lazy_image_style\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Track supported lazy images' filter.
 *
 * @Filter(
 *   id = "lazy_image_style",
 *   title = @Translation("Track supported lazy images"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE,
 *   description = @Translation("Attaching lazy load library if data-src attribute found."),
 *   weight = 10
 * )
 */
class LazyImageStyle extends FilterBase implements ContainerFactoryPluginInterface {

  /**
   * The lazy image style helper service.
   *
   * @var \Drupal\lazy_image_style\LazyImageStyleHelper
   */
  protected $helper;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->helper = $container->get('lazy_image_style.helper');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $result = new FilterProcessResult($text);

    if (stristr($text, 'data-use-lazy') === FALSE) {
      return $result;
    }
    $dom = Html::load($text);

    /** @var \DOMElement $item */
    foreach ($dom->getElementsByTagName('img') as $item) {
      if ($item->hasAttribute('data-use-lazy')) {
        $item->setAttribute('data-srcset', $item->getAttribute('src'));
        $item->setAttribute('srcset', $this->helper::TMP_BASE64_IMAGE);
        $item->setAttribute('class', $item->getAttribute('class') . ' lazyload');
        $item->removeAttribute('data-use-lazy');
      }
    }
    $result
      ->setProcessedText(Html::serialize($dom))
      ->addAttachments($this->helper->addAttachments());

    return $result;
  }

}
