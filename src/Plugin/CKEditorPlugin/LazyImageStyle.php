<?php

namespace Drupal\lazy_image_style\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "Lazy image style" plugin.
 *
 * @CKEditorPlugin(
 *   id = "lazy_image_style",
 *   label = @Translation("Lazy image style"),
 *   module = "lazy_image_style"
 * )
 */
class LazyImageStyle extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return drupal_get_path('module', 'lazy_image_style') . '/assets/js/plugins/lazy-image-style/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    $module_path = drupal_get_path('module', 'lazy_image_style');
    return [
      'lazy-image-style' => [
        'label' => $this->t('Lazy image style'),
        'image' => $module_path . '/assets/js/plugins/lazy-image-style/icons/lazy-image-style.png',
      ],
    ];
  }

}
