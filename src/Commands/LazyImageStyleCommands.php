<?php

namespace Drupal\lazy_image_style\Commands;

use Drush\Commands\DrushCommands;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Provider drush commands for lazy_image_style module.
 */
class LazyImageStyleCommands extends DrushCommands {

  /**
   * Command load lazysizes library.
   *
   * @command lazy-image-style-download
   * @aliases lis-d
   */
  public function download() {
    $fs = new Filesystem();
    $save_dir = DRUPAL_ROOT . '/libraries/lazysizes';

    if ($fs->exists($save_dir . '/lazysizes.min.js')) {
      $this->io()->success('The lazysizes library already exist.');
      return;
    }

    if (!$fs->exists($save_dir)) {
      $fs->mkdir($save_dir);
    }

    $file = file_get_contents('https://raw.githubusercontent.com/aFarkas/lazysizes/5.2.0/lazysizes.min.js');
    if (file_put_contents($save_dir . '/lazysizes.min.js', $file)) {
      $this->io()->success('The lazysizes library download.');
      return;
    }
    $this->io()->error('Could not download lazysizes library.');
  }

}
