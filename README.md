# Lazy Image Style

This module add support lazy image for image styles.


Form settings preview

![Скрин настроек](https://i.imgur.com/vAOakfn.png)

INTRODUCTION
------------

After install module goto /admin/config/media/lazy_image_settings
and select image style or responsive image style for support lazy image.

If need lazy image in CKEditor, please add "Track supported lazy images" filter.
Recommended use Full HTML text format else make sure next attributes: `data-use-lazy srcset data-srcset class` allowed for `img` tag.
